extends Node

const MOVEMENT_SPEED = 4;
const MOUSE_SENSITIVITY = 0.001;

const MIN_X_ROT = deg2rad(-70)
const MAX_X_ROT = deg2rad(70)

onready var cameras = [ $Camera, $IntersectionViewport/Camera2 ]

var camera_rot = Vector2(deg2rad(-35.0), deg2rad(-30.0))

func _ready():
	var intersection_texture = $IntersectionViewport.get_texture()
	$IntersectionDisplay.get_surface_material(0).albedo_texture = intersection_texture
	$Cube.get_surface_material(0).set_shader_param("intersection_texture", intersection_texture);

func _process(delta):
	_process_camera_movement(delta)

func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		_process_camera_rotation(event)
	elif event is InputEventMouseButton:
		if event.pressed:
			_toggle_mouse_captured()
	elif event is InputEventKey:
		if event.pressed and event.scancode == KEY_ESCAPE:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func _toggle_mouse_captured():
	var new_mode = Input.MOUSE_MODE_CAPTURED if Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED else Input.MOUSE_MODE_VISIBLE
	Input.set_mouse_mode(new_mode)


func _process_camera_movement(delta):
	var movement_vector = Vector3(0.0, 0.0, 0.0);
	if Input.is_action_pressed("camera_forward"):
		movement_vector.z -= 1.0
	if Input.is_action_pressed("camera_backwards"):
		movement_vector.z += 1.0
	if Input.is_action_pressed("camera_strafe_left"):
		movement_vector.x -= 1.0
	if Input.is_action_pressed("camera_strafe_right"):
		movement_vector.x += 1.0

	if movement_vector != Vector3(0.0, 0.0, 0.0):
		#Rotate the vector by where the camera is facing
		movement_vector = _get_camera_transform_basis().xform(movement_vector)

	if Input.is_action_pressed("camera_up"):
		movement_vector.y += 1.0
	if Input.is_action_pressed("camera_down"):
		movement_vector.y -= 1.0

	movement_vector = movement_vector.normalized()

	_set_camera_position(_get_camera_position() + movement_vector * delta * MOVEMENT_SPEED)

func _process_camera_rotation(mouse_move_event):
	var movement = mouse_move_event.relative

	var camera_transform_basis = _get_camera_transform_basis()
	camera_rot.y = wrapf(camera_rot.y + -movement.x * MOUSE_SENSITIVITY, 0.0, 2 * PI)
	camera_rot.x = clamp(camera_rot.x + -movement.y * MOUSE_SENSITIVITY, MIN_X_ROT, MAX_X_ROT)
	var new_basis = Basis(Vector3(camera_rot.x, camera_rot.y, 0.0))
	_set_camera_transform_basis(new_basis)

func _get_camera():
	return cameras[0]

func _get_camera_transform_basis():
	return _get_camera().transform.basis

func _set_camera_transform_basis(basis):
	for camera in cameras:
		camera.transform.basis = basis

func _get_camera_position():
	return _get_camera().translation

func _set_camera_position(position):
	for camera in cameras:
		camera.translation = position
