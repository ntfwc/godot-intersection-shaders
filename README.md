# Description #

This project has a shader that only colors where its mesh intersects an opaque mesh. It draws this in a viewport and uses the viewport texture to draw the final scene, rendering the intersection differently.

This could be used for some interesting effects, like having a localized x-ray or showing hidden text on walls.

# Screenshot #

![a screenshot](screenshot.png)
The cube is rendering a slightly noisy texture where it intersects the transparent spheres.

The mostly black plane displays what is seen in the secondary viewport. In its viewport, the only parts that should show up red are the intersections.

# Intersection shader explanation #

The intersection shader is actually pretty simple. We just figure out if, behind our opaque geometry, we see more back faces vs front faces. If this is true then there is an intersection.

This is implemented by using blend_add and, for fragments behind the opaque geometry depth, incrementing to a color channel for back faces and decrementing for front faces. Then only the intersecting portions will have a significant color value.

# Using it #

This is a Godot 3 project. Refer to the Godot 3 documentation for how to use the editor and launch a project.
