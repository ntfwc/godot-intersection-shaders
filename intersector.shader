shader_type spatial;
render_mode unshaded, blend_add, cull_disabled, depth_draw_never, depth_test_disable;

void fragment()
{
	float increment = 0.1;
	if(FRAGCOORD.z < textureLod(DEPTH_TEXTURE, SCREEN_UV, 0.0).r)
		discard;
	if (FRONT_FACING)
		increment = -increment;
	ALBEDO = vec3(increment, 0.0, 0.0);
}