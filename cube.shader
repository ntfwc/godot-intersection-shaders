shader_type spatial;

uniform sampler2D texture1;
uniform vec4 modulation : hint_color;
uniform sampler2D intersection_texture;

void fragment()
{
	if (textureLod(intersection_texture, SCREEN_UV, 0.0).r > 0.001)
		ALBEDO = texture(texture1, UV).rgb * modulation.rgb;
	else
		ALBEDO = vec3(0.2, 0.0, 0.0);
}